const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 

function videoGamer(objUsers){
    let res = Object.entries(objUsers)
                    .filter(user => {
                        let gamer = user[1].interests[0].split(',')
                                                .filter(interest => interest.toLowerCase().includes('video games'))
                        if(gamer.length){
                            return true
                        }else{
                            false
                        }
                    })
    return res
}

//console.log(videoGamer(users))

function germans(objUsers){
    let res = Object.entries(objUsers)
                        .filter(user => user[1].nationality === 'Germany')
    return res
}
//console.log(germans(users))

function sort(objUsers){
    let res = Object.entries(objUsers)
                        .sort((user1,user2 )=> {
                            let dsgn1 = user1[1].desgination
                            let age1 = user1[1].age
                            let dsgn2 = user2[1].desgination
                            let age2 = user2[1].age
                            let arr = ['Intern','Developer','Senior']

                        })
}

function masters(objUsers){
    let res = Object.entries(objUsers)
                        .filter(user => user[1].qualification === 'Masters')
    return res
}

//console.log(masters(users))

function technology(objUsers){
    let res = Object.entries(objUsers)
                        .reduce((accumulator,currentvalue)=>{
                            let tech = currentvalue[1].desgination
                                                        .replace('Intern','')
                                                        .replace('Senior','')
                                                        .replace('-','')
                                                        .replace('Developer','')
                                                        .trim()
                            if(accumulator.hasOwnProperty(tech)){
                                accumulator[tech].push(currentvalue[0])
                            }else{
                                accumulator[tech] = [currentvalue[0]]
                            }
                            return accumulator
                        },{})
    return res
}
console.log(technology(users))
