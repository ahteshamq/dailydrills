
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/



function fetchUser() {
    fetch('https://jsonplaceholder.typicode.com/users')
        .then(function (response) {
            if (!response.ok) {
                throw new Error("Response not recieved.")
            } else {
                return response.json()
            }
        })
        .then(function (data) {
            console.log(data)
        })
        .catch(function (err) {
            console.error(err)
        })
}

//fetchUser()

function fetchTodos() {
    fetch('https://jsonplaceholder.typicode.com/todos')
        .then(function (response) {
            if (!response.ok) {
                throw new Error("Response not recieved.")
            } else {
                return response.json()
            }
        })
        .then(function (data) {
            console.log(data)
        })
        .catch(function (err) {
            console.error(err)
        })
}

//fetchTodos()

function chainUserTodos() {
    fetch('https://jsonplaceholder.typicode.com/users')
        .then(function (responseUser) {
            if (!responseUser.ok) {
                throw new Error("Response not recieved.")
            } else {
                return responseUser.json()
            }
        })
        .then(function (user) {
            console.log(user)
        })
        .then(function () {
            return fetch('https://jsonplaceholder.typicode.com/todos')
        })
        .then(function (responseTodos) {
            if (!responseTodos.ok) {
                throw new Error("Response not recieved.")
            } else {
                return responseTodos.json()
            }
        })
        .then(function (todos) {
            console.log(todos)
        })
        .catch(function (err) {
            console.error(err)
        })
}

//chainUserTodos()

function userDetail() {
    fetch('https://jsonplaceholder.typicode.com/users')
        .then(function (response) {
            if (!response.ok) {
                throw new Error("Response not recieved.")
            } else {
                return response.json()
            }
        })
        .then(function (data) {
            data.forEach(user => {
                let url = `https://jsonplaceholder.typicode.com/users?id=${user.id}`
                fetch(url)
                    .then(function (responseID) {
                        if (!responseID.ok || responseID.length === 0) {
                            throw new Error("Response not recieved.")
                        } else {
                            return responseID.json()
                        }
                    })
                    .then(function (details) {
                        console.log(details)
                    })
            });
        })
        .catch(function (err) {
            console.error(err)
        })
}

//userDetail()

function todoUserDetail() {
    fetch('https://jsonplaceholder.typicode.com/todos')
        .then(function (response) {
            if (!response.ok) {
                throw new Error("Response not recieved.")
            } else {
                return response.json()
            }
        })
        .then(function (data) {
            console.log(data[0])
            let url = `https://jsonplaceholder.typicode.com/users?id=${data[0].userId}`
            fetch(url)
                .then(function (responseID) {
                    if (!responseID.ok || responseID.length === 0) {
                        throw new Error("Response not recieved.")
                    } else {
                        return responseID.json()
                    }
                })
                .then(function (details) {
                    console.log(details)
                })
        })
        .catch(function (err) {
            console.error(err)
        })
}

todoUserDetail()