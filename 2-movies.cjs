const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 

function club500M(objMovie){
    let res = Object.entries(objMovie).filter(movie => Number(movie[1].totalEarnings.slice(1,-1)) > 500)
                                        .map(movie => movie)
    return res
}

//console.log(club500M(favouritesMovies))

function club500M3Oscar(objMovie){
    let res = Object.entries(objMovie).filter(movie => Number(movie[1].totalEarnings
                                                                        .replace('$','')
                                                                        .replace('M','')) > 500 
                                                                        && movie[1].oscarNominations > 3)
                                        .map(movie => movie)
    return res
}
//console.log(club500M3Oscar(favouritesMovies))

function movieLeonardo(objMovie){
    let res = Object.entries(objMovie)
                        .filter(movie => {
                            let flag = movie[1].actors.filter(actor => actor === 'Leonardo Dicaprio')
                            return flag.length
                        })
    return res
}

//console.log(movieLeonardo(favouritesMovies))

function sortIMDb(objMovie){
    let res = Object.entries(objMovie)
                        .sort((movie1,movie2) => {
                            if(movie1[1].imdbRating>movie2[1].imdbRating){
                                return -1
                            }else if(movie1[1].imdbRating<movie2[1].imdbRating){
                                return 1
                            }else{
                                let earn1 = Number(movie1[1].totalEarnings.slice(1,-1))
                                let earn2 = Number(movie2[1].totalEarnings.slice(1,-1))
                                return earn2-earn1
                            }
                        })
    return res
}

console.log(sortIMDb(favouritesMovies))

function genre(objMovie){
    let res = Object.entries(favouritesMovies)
                        
}
