const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 

function available(arrayIn){
    let res = arrayIn.filter(item => item.available === true)
                        .map(res => res.name)

    return res
}

//console.log(available(items))

function vitaminC(arrayIn){
        let res = arrayIn.filter(item => {
            let vitamin =  item.contains.split(',')
            if (vitamin.length == 1 && vitamin[0] === 'Vitamin C'){

                return true
            }
             }).map(ans => ans)
        
        return res
}

//console.log(vitaminC(items))

function vitaminA(arrayIn){
    let res = arrayIn.filter(item => item.contains.includes('Vitamin A'))
                        .map(ans => ans)
    return res
}

//console.log(vitaminA(items))

function vitaminGroup(arrayIn){
    let res = arrayIn.reduce((accumulator,currentValue) => {
        let arr = currentValue.contains.split(',')
                                .map(item => item.trim())
        arr.reduce((array,presentValue) => {
            if(accumulator.hasOwnProperty(presentValue)){
                accumulator[presentValue].push(currentValue.name)
            }else{
                accumulator[presentValue] = [currentValue.name]
                
            }
            return array
        },[])
        return accumulator
    },{})
    return res
}

//console.log(vitaminGroup(items))

function vitaminSort(arrayIn){
    let res = arrayIn.sort((itemA,itemB) => {
        let countA = itemA.contains.split(',').length
        let countB = itemB.contains.split(',').length
        console.log(countA,countB)
        return countA - countB
    })
    return res
}

console.log(vitaminSort(items))