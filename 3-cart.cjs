const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/

function price65(objProducts) {
    let productArray = Object.entries(objProducts[0])
    let res = productArray.reduce((accumulator, currentValue) => {
        if (!Array.isArray(currentValue[1])) {
            if (currentValue[1].hasOwnProperty("price")) {
                if (parseInt((currentValue[1].price).replace("$", "")) > 65) {
                    accumulator.push([currentValue[0], currentValue[1].price])
                }
            }
        } else {
            let nestedProductArr = Object.entries(currentValue[1])
            let nestedRes = nestedProductArr.reduce((accumulatorInside, currentValueInside) => {
                let nestedProductArrProducts = Object.entries(currentValueInside[1])
                if (nestedProductArrProducts[0][1].hasOwnProperty("price")) {
                    if (parseInt((nestedProductArrProducts[0][1].price).replace("$", "")) > 65) {
                        accumulatorInside.push([nestedProductArrProducts[0][0], nestedProductArrProducts[0][1].price])
                    }
                }
                console.log(accumulatorInside)
                return accumulatorInside
            }, [])
            accumulator.push(nestedRes)
        }
        return accumulator
    }, [])
    return res
}

console.log(price65(products))

function order1(objProducts) {
    let productArray = Object.entries(objProducts[0])
    let res = productArray.reduce((accumulator, currentValue) => {
        if (currentValue[1].hasOwnProperty("quantity")) {
            if (currentValue[1].quantity > 1) {
                accumulator.push([currentValue[0], currentValue[1].quantity])
            }
        } else {
            let nestedProductArr = Object.entries(currentValue[1])
            let nestedRes = nestedProductArr.reduce((accumulatorInside, currentValueInside) => {
                let nestedProductArrProducts = Object.entries(currentValueInside[1])
                if (nestedProductArrProducts[0][1].hasOwnProperty("quantity")) {
                    if (nestedProductArrProducts[0][1].quantity > 1) {
                        accumulatorInside.push([nestedProductArrProducts[0][0], nestedProductArrProducts[0][1].quantity])
                    }
                }
                return accumulatorInside
            }, [])
            accumulator.push(nestedRes)
        }
        return accumulator
    }, [])
    return res
}

//console.log(order1(products))

function fragile(objProducts) {
    let productArray = Object.entries(objProducts[0])
    let res = productArray.reduce((accumulator, currentValue) => {
        if (currentValue[1].hasOwnProperty("type")) {
            if (currentValue[1].quantity > 1) {
                accumulator.push([currentValue[0], currentValue[1].quantity])
            }
        } else {
            let nestedProductArr = Object.entries(currentValue[1])
            let nestedRes = nestedProductArr.reduce((accumulatorInside, currentValueInside) => {
                let nestedProductArrProducts = Object.entries(currentValueInside[1])
                if (nestedProductArrProducts[0][1].hasOwnProperty("type")) {
                    if (nestedProductArrProducts[0][1].quantity > 1) {
                        accumulatorInside.push([nestedProductArrProducts[0][0], nestedProductArrProducts[0][1].quantity])
                    }
                }
                return accumulatorInside
            }, [])
            accumulator.push(nestedRes)
        }
        return accumulator
    }, [])
    return res
}

//console.log(fragile(products))
