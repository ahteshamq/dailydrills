/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/


function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
    return new Promise(function (resolve, reject) {

    })
}

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/

const fs = require("fs")
const path = require("path")

function problem1() {

    let filePath1 = path.join(__dirname, "file1.txt")
    let file1Data = "This is file1"
    let filePath2 = path.join(__dirname, "file2.txt")
    let file2Data = "This is file2"


    writeFile(file1Data, filePath1)
        .then(function (status) {
            console.log(status)
        }).catch(function (err) {
            console.log(err)
        })

    writeFile(file2Data, filePath2)
        .then(function (status) {
            console.log(status)
        }).catch(function (err) {
            console.log(err)
        })

    setTimeout(() => {

        deleteFile(filePath1)
            .then(function (status) {
                console.log(status)
            }).catch(function (err) {
                console.log(err)
            })

        deleteFile(filePath2)
            .then(function (status) {
                console.log(status)
            }).catch(function (err) {
                console.log(err)
            })

    }, 2 * 1000);

}

//problem1()

const lipsumData = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quis elit nisl. Aliquam congue ut orci quis gravida. Etiam eleifend ultrices turpis, ut gravida ligula accumsan vel. Cras sagittis ex ac rutrum congue. Curabitur tristique nibh posuere nisi varius, non ullamcorper sapien venenatis. Maecenas non suscipit ex. Etiam commodo magna ullamcorper ex dignissim, sit amet lacinia dolor molestie. Morbi vitae scelerisque magna. Phasellus pulvinar, mauris id ornare consequat, elit sem fermentum nisl, eget varius sapien risus at felis."


function problem2(data) {

    let lipsumPath = path.join(__dirname, "lipsum.txt")
    writeFile(data, lipsumPath)
        .then(function (status) {
            console.log(status)
            let lipsumPath = path.join(__dirname, "lipsum.txt")
            return readFile(lipsumPath)
        }).then(function (data) {
            let newFilePath = path.join(__dirname, "newFile.txt")
            return writeFile(data, newFilePath)
        }).then(function (status) {
            console.log(status)
            let lipsumPath = path.join(__dirname, "lipsum.txt")
            return deleteFile(lipsumPath)
        }).then(function (status) {
            console.log(status)
        }).catch(function (err) {
            console.log(err)
        })
}

function writeFile(lipsumTxt, filepath) {
    return new Promise(function (resolve, reject) {
        if (typeof lipsumTxt !== 'string' || typeof filepath !== 'string') {
            reject("Invalid parameter Passed erite")
        } else {
            fs.writeFile(filepath, lipsumTxt, function (err) {
                if (err) {
                    reject(err)
                } else {
                    resolve("Data written successfully.")
                }
            })
        }
    })
}

function readFile(filepath) {
    return new Promise(function (resolve, reject) {
        if (typeof filepath !== 'string') {
            reject("Invalid parameter Passed read")
        } else {
            fs.readFile(filepath, "utf-8", function (err, data) {
                if (err) {
                    reject(err)
                } else {
                    resolve(data)
                }
            })
        }
    })
}

function deleteFile(filepath) {
    return new Promise(function (resolve, reject) {
        if (typeof filepath !== 'string') {
            reject("File could not be deleted")
        } else {
            fs.unlink(filepath, function (err) {
                if (err) {
                    reject(err)
                } else {
                    resolve("File deleted successfully.")
                }
            })
        }
    })
}

//problem2(lipsumData)


function problem3() {

    let user = "Harry"
    let key = 3
    login(user, key)
        .then(function (status) {
            console.log(status)
            return getData()
        }).then(function (status) {
            console.log(status)
        })
        .catch(function (err) {
            console.log(err)
        })
}





problem3(3)