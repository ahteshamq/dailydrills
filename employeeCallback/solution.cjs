const fs = require("fs")
const path = require("path")

const dataJsonPath = path.join(__dirname, "data.json")

function empCallbacks(dataPath) {
    readData(dataPath, function (err, data) {
        if (err) {
            console.error(err)
        } else {
            empDetail = data
            problem1(empDetail, function (err, data) {
                if (err) {
                    console.error(err)
                } else {
                    console.log(data)
                    problem2(empDetail, function (err, data) {
                        if (err) {
                            console.error(err)
                        } else {
                            console.log(data)
                            problem3(empDetail, function (err, data) {
                                if (err) {
                                    console.error(err)
                                } else {
                                    console.log(data)
                                    problem4(empDetail, function (err, data) {
                                        if (err) {
                                            console.error(err)
                                        } else {
                                            console.log(data)
                                            problem5(empDetail, function (err, data) {
                                                if (err) {
                                                    console.error(err)
                                                } else {
                                                    console.log(data)
                                                    problem6(empDetail, function (err, data) {
                                                        if (err) {
                                                            console.error(err)
                                                        } else {
                                                            console.log(data)
                                                            problem7(empDetail, function (err, data) {
                                                                if (err) {
                                                                    console.error(err)
                                                                } else {
                                                                    console.log(data)
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })


}

function readData(dataPath, callback) {
    fs.readFile(dataPath, "utf-8", function (err, data) {
        if (err) {
            callback(err)
        } else {
            let emp = JSON.parse(data)
            callback(null, emp)
        }
    })
}

function problem1(data, callback) {

    let ids = [2, 13, 23]
    let res = data.employees.filter(emp => {
        if (ids.includes(emp.id)) {
            return true
        }
    })
    let jsonPath = path.join(__dirname, "problem1.json")
    fs.writeFile(jsonPath, JSON.stringify(res), function (err) {
        if (err) {
            callback(err)
        } else {
            callback(null, res)
        }
    })
}

function problem2(data, callback) {
    let res = data.employees.reduce((accu, cur) => {
        if (accu.hasOwnProperty(cur.company)) {
            accu[cur.company].push(cur)
        } else {
            accu[cur.company] = [cur]
        }
        return accu
    }, {})
    let jsonPath = path.join(__dirname, "problem2.json")
    fs.writeFile(jsonPath, JSON.stringify(res), function (err) {
        if (err) {
            callback(err)
        } else {
            callback(null, res)
        }
    })
}

function problem3(data, callback) {
    let res = data.employees.filter(employee => {
        if (employee.company === "Powerpuff Brigade") {
            return true
        }
    })
    let jsonPath = path.join(__dirname, "problem3.json")
    fs.writeFile(jsonPath, JSON.stringify(res), function (err) {
        if (err) {
            callback(err)
        } else {
            callback(null, res)
        }
    })
}

function problem4(data, callback) {
    let res = data.employees.reduce((accu, cur) => {
        if (cur.id !== 2) {
            accu.push(cur)
        }
        return accu
    }, [])
    let jsonPath = path.join(__dirname, "problem4.json")
    fs.writeFile(jsonPath, JSON.stringify(res), function (err) {
        if (err) {
            callback(err)
        } else {
            callback(null, res)
        }
    })
}

function problem5(data, callback) {
    let res = data.employees.sort((emp1, emp2) => {
        if (emp1.name > emp2.name) {
            return 1
        } else if (emp1.name < emp2.name) {
            return -1
        } else return emp1.id - emp2.id
    })
    let jsonPath = path.join(__dirname, "problem5.json")
    fs.writeFile(jsonPath, JSON.stringify(res), function (err) {
        if (err) {
            callback(err)
        } else {
            callback(null, res)
        }
    })
}

function problem6(data, callback) {
    let obj1 = data.employees.filter((employee) => {
        if (employee.id === 93) {
            return true
        }
    })
    let obj2 = data.employees.filter((employee) => {
        if (employee.id === 92) {
            return true
        }
    })
    let ind1 = data.employees.indexOf(obj1[0])
    let ind2 = data.employees.indexOf(obj2[0])
    data.employees[ind1] = obj2[0];
    data.employees[ind2] = obj1[0];
    let jsonPath = path.join(__dirname, "problem6.json")
    fs.writeFile(jsonPath, JSON.stringify(data), function (err) {
        if (err) {
            callback(err)
        } else {
            callback(null, data)
        }
    })
}

function problem7(data, callback) {
    let res = data.employees.map(company => {
        if (company.id % 2 == 0) {
            company.birthday = new Date().toISOString().slice(0, 10);
        }
        return company
    })
    let jsonPath = path.join(__dirname, "problem7.json")
    fs.writeFile(jsonPath, JSON.stringify(res), function (err) {
        if (err) {
            callback(err)
        } else {
            callback(null, res)
        }
    })

}


empCallbacks(dataJsonPath)