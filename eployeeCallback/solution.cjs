

const fs = require("fs")
const path = require("path")

const dataJsonPath = path.join(__dirname, "data.json")

function empCallbacks(dataPath) {
    readData(dataPath, function (err, data) {
        if (err) {
            //console.log(err)
            empDetail = err
            problem1(empDetail, function (err, data) {
                if (err) {
                    console.error(err)
                } else {
                    console.log(data)
                    console.log(empDetail)
                    console.log(problem2(empDetail))
                }
            })
        } else {

        }
    })


}

function readData(dataPath, callback) {
    fs.readFile(dataPath, "utf-8", function (err, data) {
        if (err) {
            console.log("p")
            callback(err)
        } else {
            console.log("q")
            let emp = JSON.parse(data)
            callback(emp)
        }
    })
}

function problem1(data, callback) {

    let ids = [2, 13, 23]
    //console.log(data.employees)
    let res = data.employees.filter(emp => {
        //console.log(emp)
        if (ids.includes(emp.id)) {
            return true
        }
    })
    //console.log(res)
    callback(res)
}
function problem2(data) {
    console.log("hii")
    let res = data.employees.reduce((accu, cur) => {
        if (accu.hasOwnProperty(cur.company)) {
            accu[cur.company].push(cur)
        } else {
            accu[cur.company] = [cur]
        }
        return accu
    }, {})
    return res
}


empCallbacks(dataJsonPath)